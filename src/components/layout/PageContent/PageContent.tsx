import React from 'react';

export const PageContent: React.FC = ({ children }) => {
    return (
        <div className="mt-12 max-w-6xl mx-auto">{children}</div>
    );
};