import React from 'react';
import cx from 'classnames';

export enum HeaderSize {
    PRIMARY,
    SECONDARY,
    TERTIARY,
    SMALL,
}

export interface HeaderProps {
    size: HeaderSize;
    component: keyof JSX.IntrinsicElements;
    className?: string;
}

export const Header: React.FC<HeaderProps> = ({ size, component, className, children }) => {
    const Component = component as keyof JSX.IntrinsicElements;
    return (
        <Component
            className={cx(
                {
                    [`text-4xl tracking-tight font-extrabold text-gray-900 sm:text-5xl md:text-6xl`]:
                    size === HeaderSize.PRIMARY,
                    [`text-base text-primary-600 font-semibold tracking-wide uppercase`]:
                    size === HeaderSize.SECONDARY,
                    [`text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl`]:
                    size === HeaderSize.TERTIARY,
                    [`text-lg leading-6 font-medium text-gray-900`]: size === HeaderSize.SMALL,
                },
                className
            )}
        >
            {children}
        </Component>
    );
};
