import React from 'react';
import { Header, HeaderSize } from '../../typography';

export interface FormSectionProps {
    title: string;
    instruction?: string;
    showDivider?: boolean;
    actions?: Array<React.ReactElement>;
    className?: string;
}

export const FormSection: React.FC<FormSectionProps> = ({
                                                            className,
                                                            title,
                                                            instruction,
                                                            actions,
                                                            showDivider = true,
                                                            children,
                                                        }) => {
    return (
        <div className={className}>
            <div className="md:grid md:grid-cols-3 md:gap-6">
                <div className="md:col-span-1">
                    <div className="px-4 sm:px-0">
                        <Header size={HeaderSize.SMALL} component="h3">
                            {title}
                        </Header>
                        <p className="mt-1 text-sm text-gray-600">{instruction}</p>
                    </div>
                </div>
                <div className="mt-5 md:mt-0 md:col-span-2">
                    <div className="shadow overflow-hidden sm:rounded-md">
                        <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                            <div className="grid grid-cols-6 gap-6">{children}</div>
                        </div>
                        {actions && (
                            <div className="px-4 py-3 bg-gray-50 flex flex-row-reverse sm:px-6 gap-x-2">
                                {actions}
                            </div>
                        )}
                    </div>
                </div>
            </div>
            {showDivider && (
                <div className="hidden sm:block" aria-hidden="true">
                    <div className="py-5">
                        <div className="border-t border-gray-200" />
                    </div>
                </div>
            )}
        </div>
    );
};
