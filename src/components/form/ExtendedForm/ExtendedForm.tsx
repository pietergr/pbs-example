import React from 'react';
import { Form } from 'react-final-form';
import { FormApi, ValidationErrors } from 'final-form';

export interface ExtendedFormProps<FormValues> {
    onSubmit: (values: FormValues, form: FormApi<FormValues, Partial<FormValues>>) => void;
    validate?: (values: FormValues) => ValidationErrors | Promise<ValidationErrors>;
}

export function ExtendedForm<FormValues>({
                                             onSubmit,
                                             validate,
                                             children,
                                         }: React.PropsWithChildren<ExtendedFormProps<FormValues>>): React.ReactElement | null {
    return (
        <Form
            onSubmit={onSubmit}
            validate={validate}
            render={({ handleSubmit }) => {
                return (
                    <form onSubmit={handleSubmit} noValidate>
                        {children}
                    </form>
                );
            }}
        />
    );
}
