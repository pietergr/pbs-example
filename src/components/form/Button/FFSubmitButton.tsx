import React from 'react';
import { Button, ButtonProps, ButtonSize, ButtonVariant } from './Button';
import { useForm } from 'react-final-form';

interface Props {
    formId: string;
}

export const FFSubmitButton: React.FC<ButtonProps & Props> = ({ formId, children }) => {
    const form = useForm();

    return (
        <Button
            variant={ButtonVariant.DARK}
            size={ButtonSize.SMALL}
            onClick={(e?: React.MouseEvent<HTMLButtonElement>) => {
                e?.preventDefault();
                form.submit();
            }}
            data-testid={`${formId}-submit`}
        >
            {children}
        </Button>
    );
};
