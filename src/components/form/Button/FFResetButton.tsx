import React from 'react';
import { Button, ButtonSize, ButtonVariant, ButtonProps } from './Button';
import { useForm } from 'react-final-form';

interface Props {
    formId: string;
}

export const FFResetButton: React.FC<ButtonProps & Props> = ({ formId, children}) => {
    const form = useForm();
    const [dirty, setDirty] = React.useState<boolean>(false);

    React.useEffect(() => {
        return form.subscribe(({ dirty: isDirty }) => setDirty(isDirty), { dirty: true });
    }, [form]);

    return (
        <Button
            variant={ButtonVariant.LIGHT}
            size={ButtonSize.SMALL}
            onClick={(e?: React.MouseEvent<HTMLButtonElement>) => {
                e?.preventDefault();
                form.reset({});
            }}
            disabled={!dirty}
            data-testid={`${formId}-reset`}
        >
            {children}
        </Button>
    );
};
