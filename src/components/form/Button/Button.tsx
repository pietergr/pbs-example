import React from 'react';
import cx from 'classnames';

export enum ButtonVariant {
    DEFAULT,
    DARK,
    LIGHT,
}

export enum ButtonSize {
    SMALL,
    MEDIUM,
    LARGE,
}

export interface ButtonProps {
    onClick?: (event?: React.MouseEvent<HTMLButtonElement>) => void;
    variant?: ButtonVariant;
    type?: 'button' | 'submit' | 'reset';
    size?: ButtonSize;
}

export const Button: React.FC<ButtonProps & React.HTMLProps<HTMLButtonElement>> = ({
                                                                                       children,
                                                                                       disabled,
                                                                                       variant = ButtonVariant.DEFAULT,
                                                                                       size = ButtonSize.MEDIUM,
                                                                                       ...props
                                                                                   }) => (
    <button
        className={cx(
            `rounded-md shadow 
            flex items-center 
            justify-center border 
            border-transparent text-base font-medium 
            `,
            {
                [`text-primary-600 bg-white hover:bg-primary-50`]:
                variant === ButtonVariant.DEFAULT && !disabled,
            },
            {
                [`text-white bg-primary-600 hover:bg-primary-700`]:
                variant === ButtonVariant.DARK && !disabled,
            },
            {
                [`text-primary-700 bg-primary-100 hover:bg-primary-200`]:
                variant === ButtonVariant.LIGHT && !disabled,
            },
            { [`py-2 px-3 leading-4`]: size === ButtonSize.SMALL },
            { [`py-3 px-5`]: size === ButtonSize.MEDIUM },
            { [`px-8 py-3 md:py-4 md:text-lg md:px-10`]: size === ButtonSize.LARGE },
            { [`cursor-pointer`]: !disabled },
            { [`bg-gray-100 cursor-default text-gray-300`]: disabled }
        )}
        disabled={disabled}
        {...props}
    >
        {children}
    </button>
);
