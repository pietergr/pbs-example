import React from 'react';
import { MarkdownInput, MarkdownInputProps } from './MarkdownInput';
import { useField } from 'react-final-form';

type Props = Omit<MarkdownInputProps, 'value' | 'onChange' | 'error'>;

export const FFMarkdownInput: React.FC<Props> = ({ name, ...props }) => {
    const {
        input: { value, onChange },
        meta: { error, submitFailed },
    } = useField(name);

    return <MarkdownInput name={name} onChange={onChange} value={value} error={submitFailed ? error : undefined} {...props} />;
};
