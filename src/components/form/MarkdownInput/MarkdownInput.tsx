import React from 'react';
import cx from 'classnames';
import MarkdownIt from 'markdown-it';
import 'react-markdown-editor-lite/lib/index.css';
import MdEditor from 'react-markdown-editor-lite'

interface Props {
    className?: string;
    name: string;
    label: string;
    required?: boolean;
    error?: string;
    helperText?: string;
    value: string;
    onChange: (value?: string) => void;
    hideToolbar?: boolean;
}

export type MarkdownInputProps = Props &
    Omit<
        React.TextareaHTMLAttributes<HTMLTextAreaElement>,
        'name' | 'label' | 'required' | 'value' | 'onChange' | 'onScroll'
        >;

const mdParser = new MarkdownIt();

export const MarkdownInput: React.FC<MarkdownInputProps> = ({
                                                                className,
                                                                label,
                                                                name,
                                                                required,
                                                                error,
                                                                helperText,
                                                                value,
                                                                onChange,
                                                                placeholder,
                                                            }) => {
    const config = {
        view: {
            menu: true,
            md: true,
            html: false,
        },
        markdownClass: cx(
            'border-gray-300 bg-gray-100! shadow-inner',
            { 'focus:ring-primary-500 focus:border-primary-500 border-gray-300': !error },
            { 'focus:ring-red-700 focus:border-red-700 border-red-700': error }
        ),
    };
    const handleChange = ({ text }: { html: string; text: string }): void => onChange(text);
    return (
        <div className={cx(className)} data-testid={`${name}-text-area`}>
            <label
                htmlFor={`${name}-text-field`}
                className={cx(
                    'mb-1 block text-sm font-medium',
                    { 'text-gray-700': !error },
                    { 'text-red-700': error }
                )}
            >
                {label}
                {required && <span className="text-red-700 ml-1">*</span>}
            </label>
            <MdEditor
                onChange={handleChange}
                value={value}
                renderHTML={(text) => mdParser.render(text)}
                config={config}
                placeholder={placeholder}
                style={{ height: '200px' }}
            />
            {!error && helperText && <p className="mt-2 text-sm text-gray-500">{helperText}</p>}
            {error && <p className="mt-2 text-sm text-red-700">{error}</p>}
        </div>
    );
};
