import React from 'react';
import cx from 'classnames';

interface Option {
    id: string;
    label: string;
}

interface RadioOptionProps extends Option {
    name: string;
    checked?: boolean;
    onChange: (value: string) => void;
}

export interface Props {
    label: string;
    instruction?: string;
    name: string;
    options: Array<Option>;
    value: string;
    onChange: (value: string) => void;
    error?: string;
    required?: boolean;
}

const RadioOption: React.FC<RadioOptionProps> = ({ id, label, name, checked, onChange }) => {
    const handleChange = (): void => onChange(id);
    return (
        <div className="flex items-center">
            <input
                id={id}
                data-testid={id}
                name={name}
                type="radio"
                checked={checked}
                className="focus:ring-primary-500 h-4 w-4 text-primary-600 border-gray-300"
                onChange={handleChange}
            />
            <label
                htmlFor="push_everything"
                className="ml-3 block text-sm font-medium text-gray-700"
            >
                {label}
            </label>
        </div>
    );
};

export const RadioInput: React.FC<Props> = ({ label, name, instruction, options, value, onChange, error, required }) => {

    return (
        <fieldset>
            <div>
                <label
                    htmlFor={`${name}-text-field`}
                    className={cx(
                        'block text-sm font-medium',
                        { 'text-gray-700': !error },
                        { 'text-red-700': error }
                    )}
                >
                    {label}
                    {required && <span className="text-red-700 ml-1">*</span>}
                </label>
                {instruction && <p className="text-sm text-gray-500">
                    {instruction}
                </p>}
            </div>
            <div className="mt-4 space-y-4">
                {options.map(({ id, label }) => (
                    <RadioOption key={id} id={id} label={label} name={name} onChange={onChange} checked={value === id}/>
                ))}
            </div>
            {error && <p className="mt-2 text-sm text-red-700">{error}</p>}
        </fieldset>
    );
};