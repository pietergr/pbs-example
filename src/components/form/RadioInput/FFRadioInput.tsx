import React from 'react';
import { RadioInput, Props } from './RadioInput';
import { useField } from 'react-final-form';

type FFRadioInputProps = Omit<Props, 'value' | 'onChange' | 'error'>;

export const FFRadioInput: React.FC<FFRadioInputProps> = ({ name, ...props }) => {
    const {
        input: { value, onChange },
        meta: { error, submitFailed },
    } = useField(name);

    return (
        <RadioInput {...props} name={name} onChange={onChange} error={submitFailed ? error : undefined} value={value}/>
    );
};