import React from 'react';
import cx from 'classnames';

export interface FieldSectionProps {
    defaultColSpan?: number;
    smallColSpan?: number;
    largeColSpan?: number;
    className?: string;
}

export const FieldSection: React.FC<FieldSectionProps> = ({
                                                              className,
                                                              largeColSpan,
                                                              children,
                                                              defaultColSpan = 6,
                                                              smallColSpan = 6,
                                                          }) => (
    <div
        className={cx(className, `col-span-${defaultColSpan}`, `sm:col-span-${smallColSpan}`, {
            [`lg:col-span-${largeColSpan}`]: largeColSpan,
        })}
    >
        {children}
    </div>
);
