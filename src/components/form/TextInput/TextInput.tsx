import React from 'react';
import cx from 'classnames';

interface Props {
    label: string;
    name: string;
    value: string;
    onChange: (value: string) => void;
    className?: string;
    error?: string;
    helperText?: string;
    required?: boolean;
    type?: string;
}

export type TextInputProps = Omit<React.HTMLProps<HTMLInputElement>,
    'label' | 'name' | 'value' | 'onChange' | 'type'> &
    Props;

export const TextInput: React.FC<TextInputProps> = ({
                                                        className,
                                                        label,
                                                        name,
                                                        value,
                                                        onChange,
                                                        error,
                                                        helperText,
                                                        required,
                                                        type = 'text',
                                                        ...props
                                                    }) => {
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void =>
        onChange(event.target.value);

    return (
        <div className={className}>
            <label
                htmlFor={`${name}-text-field`}
                className={cx(
                    'block text-sm font-medium',
                    { 'text-gray-700': !error },
                    { 'text-red-700': error }
                )}
            >
                {label}
                {required && <span className="text-red-700 ml-1">*</span>}
            </label>
            <input
                type={type}
                name={name}
                id={`${name}-text-input`}
                data-testid={`${name}-text-input`}
                onChange={handleChange}
                value={value}
                required={required}
                className={cx(
                    'bg-gray-100 shadow-inner',
                    'mt-1  block w-full shadow-sm sm:text-sm border-gray-300 rounded-md',
                    { 'focus:ring-primary-500 focus:border-primary-500 border-gray-300': !error },
                    { 'focus:ring-red-700 focus:border-red-700 border-red-700': error }
                )}
                {...props}
            />
            {!error && helperText && <p className="mt-2 text(sm gray-500)">{helperText}</p>}
            {error && <p className="text-sm text-red-700">{error}</p>}
        </div>
    );
};
