import React from 'react';
import { useField } from 'react-final-form';
import { TextInput, TextInputProps } from './TextInput';

type Props = Omit<TextInputProps, 'value' | 'onChange' | 'error'>;

export const FFTextInput: React.FC<Props> = ({ name, ...props }) => {
    const {
        input: { value, onChange },
        meta: { error, submitFailed },
    } = useField(name);

    return (
        <TextInput
            name={name}
            value={value}
            onChange={onChange}
            error={submitFailed ? error : undefined}
            {...props}
        />
    );
};
