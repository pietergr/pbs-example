import { ObjectShape, OptionalObjectSchema } from 'yup/lib/object';
import { setIn, ValidationErrors } from 'final-form';
import * as yup from 'yup';

export const validateFormValues = <T>(
    schema: OptionalObjectSchema<ObjectShape>
) => async (values: T): Promise<ValidationErrors> => {
    try {
        await schema.validate(values, { abortEarly: false });
    } catch (err) {
        return err.inner.reduce(
            (formError: Record<string, unknown>, innerError: yup.ValidationError) => {
                return setIn(formError, innerError.path ?? '', innerError.message);
            },
            {}
        );
    }
};
