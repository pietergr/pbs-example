import { createMachine } from 'xstate';
import { createModel } from '@xstate/test';
import { fireEvent, act, cleanup, render, waitFor } from '@testing-library/react';
import { PersonalDetails } from './PersonalDetails';

describe('Personal Details Form', () => {

    const personalDetailsMachine = createMachine({
        id: 'personaldetails',
        initial: 'pristine',
        states: {
            pristine: {
                on: {
                    SUBMIT_WITHOUT_FULL_NAME: 'fullNameValidationFails',
                    SUBMIT_WITHOUT_EMAIL: 'emailValidationFails',
                    SUBMIT_WITH_WRONG_EMAIL: 'emailFormatValidationFails',
                    SUBMIT_WITHOUT_WORK_STATUS: 'workStatusValidationFails',
                },
                meta: {
                    test: ({ getByTestId }) => {
                        expect(getByTestId('fullName-text-input')).toHaveValue('');
                        expect(getByTestId('email-text-input')).toHaveValue('');
                        expect(getByTestId('working')).not.toBeChecked();
                        expect(getByTestId('notWorking')).not.toBeChecked();
                    },
                },
            },
            fullNameValidationFails: {
                on: {
                    RESET: 'pristine',
                },
                meta: {
                    test: async ({ getByText }) => {
                        await waitFor(() => expect(getByText('Your full name is required')).toBeInTheDocument());
                    },
                },
            },
            emailValidationFails: {
                on: {
                    RESET: 'pristine',
                },
                meta: {
                    test: async ({ getByText }) => {
                        await waitFor(() => expect(getByText('Your email address is required')).toBeInTheDocument());
                    },
                },
            },
            emailFormatValidationFails: {
                on: {
                    RESET: 'pristine',
                },
                meta: {
                    test: async ({ getByText }) => {
                        await waitFor(() => expect(getByText('This does not look like a valid email. Typo?')).toBeInTheDocument());
                    },
                },
            },
            workStatusValidationFails: {
                on: {
                    RESET: 'pristine',
                },
                meta: {
                    test: async ({ getByText }) => {
                        await waitFor(() => expect(getByText('You did not make a selection here')).toBeInTheDocument());
                    },
                },
            },
        },
    });

    const personalDetailsModel = createModel(personalDetailsMachine, {
        events: {
            SUBMIT_WITHOUT_FULL_NAME: ({ getByTestId }) => {
                act(() => {
                    fireEvent.change(getByTestId('email-text-input'), { target: { value: 'john.smith@email.com' } });
                    fireEvent.click(getByTestId('working'));
                    fireEvent.click(getByTestId('personalDetails-submit'));
                });
            },
            SUBMIT_WITHOUT_EMAIL: ({ getByTestId }) => {
                act(() => {
                    fireEvent.change(getByTestId('fullName-text-input'), { target: { value: 'John Smith' } });
                    fireEvent.click(getByTestId('working'));
                    fireEvent.click(getByTestId('personalDetails-submit'));
                });
            },
            SUBMIT_WITH_WRONG_EMAIL: ({ getByTestId }) => {
                act(() => {
                    fireEvent.change(getByTestId('fullName-text-input'), { target: { value: 'John Smith' } });
                    fireEvent.change(getByTestId('email-text-input'), { target: { value: 'invalid_email' } });
                    fireEvent.click(getByTestId('working'));
                    fireEvent.click(getByTestId('personalDetails-submit'));
                });
            },
            SUBMIT_WITHOUT_WORK_STATUS: ({ getByTestId }) => {
                act(() => {
                    fireEvent.change(getByTestId('fullName-text-input'), { target: { value: 'John Smith' } });
                    fireEvent.change(getByTestId('email-text-input'), { target: { value: 'john.smith@email.com' } });
                    fireEvent.click(getByTestId('personalDetails-submit'));
                });
            },
            RESET: ({ getByTestId }) => {
                act(() => {
                    fireEvent.click(getByTestId('personalDetails-reset'));
                });
            },
        },
    });

    const testPlans = personalDetailsModel.getSimplePathPlans();
    testPlans.forEach((plan) => {
        describe(plan.description, () => {
            afterEach(cleanup);
            plan.paths.forEach((path) => {
                it(path.description, () => {
                    const rendered = render(<PersonalDetails onChangePersonalDetails={jest.fn()}/>);
                    return path.test(rendered);
                });
            });
        });
    });

    it('coverage', () => {
        personalDetailsModel.testCoverage();
    });

});
