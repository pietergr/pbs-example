import * as yup from 'yup';

export const personalDetailsSchema = yup.object().shape({
    fullName: yup.string().required('Your full name is required'),
    email: yup.string().email('This does not look like a valid email. Typo?').required('Your email address is required'),
    workStatus: yup.string().required('You did not make a selection here'),
});
