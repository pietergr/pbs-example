import React from 'react';
import {
    ButtonSize,
    ButtonVariant,
    ExtendedForm, FFRadioInput, FFResetButton,
    FFSubmitButton, FFTextInput, FieldSection,
    FormSection,
} from '../../components';
import { validateFormValues } from '../../utils';
import { personalDetailsSchema } from './PersonalDetails.schema';

export enum WorkStatus {
    WORKING = 'working',
    NOT_WORKING = 'notWorking',
}

export interface PersonalDetailsValues {
    fullName: string;
    email: string;
    workStatus: WorkStatus;
}

interface Props {
    onChangePersonalDetails: (values: PersonalDetailsValues) => void;
}

export const PersonalDetails: React.FC<Props> = ({ onChangePersonalDetails }) => {

    return (
        <ExtendedForm<PersonalDetailsValues>
            onSubmit={onChangePersonalDetails}
            validate={validateFormValues<PersonalDetailsValues>(personalDetailsSchema)}>
            <FormSection title="Who you are" instruction="Give us some details about yourself and how we can contact you" actions={[
                <FFSubmitButton key="continue" variant={ButtonVariant.DARK}
                                size={ButtonSize.SMALL} formId="personalDetails">Continue</FFSubmitButton>,
                <FFResetButton key="reset" variant={ButtonVariant.DEFAULT}
                               size={ButtonSize.SMALL} formId="personalDetails">Reset</FFResetButton>
            ]}>
                <FieldSection smallColSpan={4}>
                    <FFTextInput label="Your full name" name="fullName" required/>
                </FieldSection>
                <FieldSection smallColSpan={4}>
                    <FFTextInput label="Your email address" name="email" type="email" required/>
                </FieldSection>
                <FieldSection>
                    <FFRadioInput required label="Your work status" instruction="Are you currently working for Port Blue Sky?"
                                  name="workStatus" options={[
                        { id: WorkStatus.NOT_WORKING, label: `No, I'm not currently working for PBS` },
                        { id: WorkStatus.WORKING, label: `Yes, I'm working for PBS` },
                    ]}/>
                </FieldSection>
            </FormSection>
        </ExtendedForm>
    );
};