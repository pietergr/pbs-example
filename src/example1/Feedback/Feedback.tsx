import React from 'react';
import {
    ButtonSize,
    ButtonVariant,
    ExtendedForm, FFMarkdownInput, FFResetButton,
    FFSubmitButton, FFTextInput, FieldSection,
    FormSection,
} from '../../components';
import { validateFormValues } from '../../utils';
import { feedbackSchema } from './Feedback.schema';

export interface FeedbackValues {
    lineManager: string;
    feedback: string;
}

interface Props {
    onChangeFeedback: (values: FeedbackValues) => void;
}

export const Feedback: React.FC<Props> = ({ onChangeFeedback }) => {

    return (
        <ExtendedForm<FeedbackValues>
            onSubmit={onChangeFeedback}
            validate={validateFormValues<FeedbackValues>(feedbackSchema)}>
            <FormSection title="Your feedback" instruction="We find your feedback very valuable. Please tell us about your experience at PBS" actions={[
                <FFSubmitButton key="send" variant={ButtonVariant.DARK}
                                size={ButtonSize.SMALL} formId="feedback">Send</FFSubmitButton>,
                <FFResetButton key="reset" variant={ButtonVariant.DEFAULT}
                               size={ButtonSize.SMALL} formId="feedback">Reset</FFResetButton>
            ]}>
                <FieldSection smallColSpan={4}>
                    <FFTextInput label="Who was your line manager?" name="lineManager" required/>
                </FieldSection>
                <FieldSection smallColSpan={6}>
                    <FFMarkdownInput name="feedback" label="Please provide your feedback here. You can enter markdown in this field" required placeholder="Write your feedback here" />
                </FieldSection>
            </FormSection>
        </ExtendedForm>
    );
};