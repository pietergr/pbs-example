import * as yup from 'yup';

export const feedbackSchema = yup.object().shape({
    lineManager: yup.string().required(`We need to know who your boss was`),
    feedback: yup.string().required('Please give us some feedback here before submitting'),
});
