import React from 'react';
import { PersonalDetailsValues } from './PersonalDetails';
import { FeedbackValues } from './Feedback';
import { ApplicationValues } from './Application';

export enum ActionType {
    PERSONAL,
    FEEDBACK,
    APPLICATION,
    SUBMIT,
    SUCCESS,
}

interface Action {
    type: ActionType;
    payload?: PersonalDetailsValues | FeedbackValues | ApplicationValues;
}

interface Example1State {
    details?: PersonalDetailsValues;
    feedback?: FeedbackValues;
    application?: ApplicationValues;
    isLoading?: boolean;
    isSuccess?: boolean;
}

const reducer = (state: Example1State, { type, payload }: Action): Example1State => {
    switch (type) {
        case ActionType.PERSONAL:
            return {
                ...state,
                details: payload as PersonalDetailsValues,
            };
        case ActionType.FEEDBACK:
            return {
                ...state,
                feedback: payload as FeedbackValues,
            };
        case ActionType.APPLICATION:
            return {
                ...state,
                application: payload as ApplicationValues,
            };
        case ActionType.SUBMIT:
            return {
                ...state,
                isSuccess: false,
                isLoading: true,
            };
        case ActionType.SUCCESS:
            return {
                ...state,
                isLoading: false,
                isSuccess: true,
            };
    }
    return state;
};

export const useExample1 = () => {
    const [ { details, feedback, application, isLoading, isSuccess }, dispatch] = React.useReducer(reducer, {});

    const handleUpdates = (type: ActionType) => (payload: PersonalDetailsValues | FeedbackValues | ApplicationValues): void => dispatch({
        type,
        payload,
    });

    React.useEffect(() => {
        if (!feedback && !application) {
            return;
        }
        dispatch({
            type: ActionType.SUBMIT
        });
        const timeout = setTimeout(() => dispatch({
            type: ActionType.SUCCESS,
        }), 500);
        return () => clearTimeout(timeout);
    }, [feedback, application]);

    return {
        details,
        feedback,
        application,
        isLoading,
        isSuccess,
        handleUpdates,
    };
};