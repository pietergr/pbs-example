import React from 'react';
import {
    ButtonSize,
    ButtonVariant,
    ExtendedForm, FFMarkdownInput, FFResetButton,
    FFSubmitButton, FFTextInput, FieldSection,
    FormSection,
} from '../../components';
import { validateFormValues } from '../../utils';
import { applicationSchema } from './Application.schema';

export interface ApplicationValues {
    jobTitle: string;
    coverLetter: string;
}

interface Props {
    onChangeApplication: (values: ApplicationValues) => void;
}

export const Application: React.FC<Props> = ({ onChangeApplication }) => {

    return (
        <ExtendedForm<ApplicationValues>
            onSubmit={onChangeApplication}
            validate={validateFormValues<ApplicationValues>(applicationSchema)}>
            <FormSection title="Work with us" instruction="Use this form to apply for a position" actions={[
                <FFSubmitButton key="send" variant={ButtonVariant.DARK}
                                size={ButtonSize.SMALL} formId="application">Send</FFSubmitButton>,
                <FFResetButton key="reset" variant={ButtonVariant.DEFAULT}
                               size={ButtonSize.SMALL} formId="application">Reset</FFResetButton>
            ]}>
                <FieldSection smallColSpan={4}>
                    <FFTextInput label="Which job are you applying for?" name="jobTitle" required/>
                </FieldSection>
                <FieldSection smallColSpan={6}>
                    <FFMarkdownInput name="coverLetter" label="Please introduce yourself and tell us about your skills" placeholder="Write your cover letter here" />
                </FieldSection>
            </FormSection>
        </ExtendedForm>
    );
};