import * as yup from 'yup';

export const applicationSchema = yup.object().shape({
    jobTitle: yup.string().required(`We need to know what job you are applying for`),
});
