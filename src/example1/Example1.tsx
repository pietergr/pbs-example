import React from 'react';
import { Header, HeaderSize, PageContent, Loader, SubmissionSuccess } from '../components';
import { PersonalDetails, WorkStatus } from './PersonalDetails';
import { Feedback } from './Feedback';
import { ActionType, useExample1 } from './useExample1';
import { Application } from './Application';

export const Example1: React.FC = () => {
    const { isLoading, isSuccess, details, handleUpdates } = useExample1();

    return (
        <PageContent>
            <Header className="mb-12" size={HeaderSize.PRIMARY} component="h1">
                <span className="text-primary-500">Working</span>{' '}
                <span>at Port Blue Sky</span>
            </Header>
            { !isLoading && !isSuccess && (<>
                <PersonalDetails onChangePersonalDetails={handleUpdates(ActionType.PERSONAL)} />
                {details?.workStatus === WorkStatus.WORKING && <Feedback onChangeFeedback={handleUpdates(ActionType.FEEDBACK)} />}
                {details?.workStatus === WorkStatus.NOT_WORKING && <Application onChangeApplication={handleUpdates(ActionType.APPLICATION)} />}
            </>)}
            {isLoading && <Loader />}
            {isSuccess && <SubmissionSuccess />}
        </PageContent>
    );
};