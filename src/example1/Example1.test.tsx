import { createMachine } from 'xstate';
import { createModel } from '@xstate/test';
import { fireEvent, act, cleanup, render, waitFor } from '@testing-library/react';
import { Example1 } from './Example1';

describe('Example1: Multiple form sequence', () => {

    const example1Machine = createMachine({
        id: 'example1',
        initial: 'details',
        states: {
            details: {
                on: {
                    SUBMIT_DETAILS_AS_EMPLOYEE: 'feedback',
                    SUBMIT_DETAILS_AS_CANDIDATE: 'application',
                },
                meta: {
                    test: ({ getByTestId }) => {
                        expect(getByTestId('fullName-text-input')).toBeInTheDocument();
                    },
                },
            },
            feedback: {
                on: {
                    SUBMIT_FEEDBACK: 'loading',
                },
                meta: {
                    test: async ({ getByTestId }) => {
                        await waitFor(() => expect(getByTestId('lineManager-text-input')).toBeInTheDocument());
                    },
                },
            },
            application: {
                on: {
                    SUBMIT_APPLICATION: 'loading'
                },
                meta: {
                    test: async ({ getByTestId }) => {
                        await waitFor(() => expect(getByTestId('jobTitle-text-input')).toBeInTheDocument());
                    },
                },
            },
            loading: {
                on: {
                    LOAD_SUCCESS: 'success'
                },
                meta: {
                    test: async ({ getByTestId }) => {
                        await waitFor(() => expect(getByTestId('loader')).toBeInTheDocument());
                    },
                },
            },
            success: {
                type: 'final',
                meta: {
                    test: async ({ getByTestId }) => {
                        await waitFor(() => expect(getByTestId('success')).toBeInTheDocument());
                    },
                },
            },
        },
    });

    const example1Model = createModel(example1Machine, {
        events: {
            SUBMIT_DETAILS_AS_EMPLOYEE: ({ getByTestId }) => {
                act(() => {
                    fireEvent.change(getByTestId('fullName-text-input'), { target: { value: 'John Smith' } });
                    fireEvent.change(getByTestId('email-text-input'), { target: { value: 'john.smith@email.com' } });
                    fireEvent.click(getByTestId('working'));
                    fireEvent.click(getByTestId('personalDetails-submit'));
                });
            },
            SUBMIT_DETAILS_AS_CANDIDATE: ({ getByTestId }) => {
                act(() => {
                    fireEvent.change(getByTestId('fullName-text-input'), { target: { value: 'John Smith' } });
                    fireEvent.change(getByTestId('email-text-input'), { target: { value: 'john.smith@email.com' } });
                    fireEvent.click(getByTestId('notWorking'));
                    fireEvent.click(getByTestId('personalDetails-submit'));
                });
            },
            SUBMIT_FEEDBACK: ({ getByTestId, getByPlaceholderText }) => {
                act(() => {
                    fireEvent.change(getByTestId('lineManager-text-input'), { target: { value: 'Mark Shuttleworth' } });
                    fireEvent.change(getByPlaceholderText('Write your feedback here'), { target: { value: 'This is my feedback' } });
                    fireEvent.click(getByTestId('feedback-submit'));
                });
            },
            SUBMIT_APPLICATION: ({ getByTestId, getByPlaceholderText }) => {
                act(() => {
                    fireEvent.change(getByTestId('jobTitle-text-input'), { target: { value: 'Front-end developer' } });
                    fireEvent.change(getByPlaceholderText('Write your cover letter here'), { target: { value: 'This is my cover letter' } });
                    fireEvent.click(getByTestId('application-submit'));
                });
            },
            LOAD_SUCCESS: async ({ getByTestId }) => {
                act(() => {
                    jest.useFakeTimers();
                    setTimeout(() => console.log('waiting'), 1000);
                    jest.runAllTimers();
                });
            },
        },
    });

    const testPlans = example1Model.getSimplePathPlans();
    testPlans.forEach((plan) => {
        describe(plan.description, () => {
            afterEach(cleanup);
            plan.paths.forEach((path) => {
                it(path.description, () => {
                    const rendered = render(<Example1 />);
                    return path.test(rendered);
                });
            });
        });
    });

    it('coverage', () => {
        example1Model.testCoverage();
    });

});

