import React from 'react';
import { Example1 } from './example1';

const App: React.FC = () => (
    <Example1 />
);

export default App;
